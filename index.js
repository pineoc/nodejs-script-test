const fs = require('fs');

function logTest () {
  let warnCnt = 0;
  console.log('log');
  console.error('log error check');
  console.warn('log warn check');
  warnCnt += 1;
  console.warn('log warn check2');
  warnCnt += 1;
  warnCnt += 1;
  return warnCnt;
}

process.on('exit', function(code) {  
  return console.log(`About to exit with code ${code}`);
});

module.exports = {
  logTest
}