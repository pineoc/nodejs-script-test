const assert = require('assert');
const index = require('../index');

describe('test function, Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal([1,2,3].indexOf(4), -1);
    });
  });
});

describe('index function test', function() {
  describe('warn check', function() {
    it('should return 2', function() {
      assert.equal(index.logTest(), 2);
    });
  });
});